class Employee {

    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }
    get age() {
        return this._age;
    }
    get salary() {
        return this._salary;
    }


    set name(newName) {
        this._name = newName;
    }
    set age(newAge) {
        this._age = newAge;
    }
    set salary(newSalary) {
        this._salary = newSalary;
    }
}

class Programmer extends Employee {

    constructor(name, age, salary, langArr) {
        super(name, age, salary);
        this.languages = langArr;
    }

    get salary() {
        return this._salary * 3;
    }

}

const mary = new Programmer('Mary', 30, 2000, ['php', 'js', 'java']);
const bob = new Programmer('Bob', 40, 4000, ['js', 'css', 'java']);
const james = new Programmer('James', 35, 3000, ['css', 'js', 'html']);


console.log(mary);
console.log(bob);
console.log(james);
