const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

// Фрагмент для пунктов списка
let fragment = document.createDocumentFragment();

// Создание и вывод списка на страницу
function createList(parentEl, arr) {
    let ul = document.createElement("ul");
    arr.forEach(book => {
        if (checkBookInfo(book)) {
            return createBook(book)
        }
    })

    ul.append(fragment);
    parentEl.append(ul);
}

// Создание пункта списка
function createBook(bookObj) {

    let li = document.createElement("li");
    li.innerHTML =
        `<h2>${bookObj.name}</h2>
         <p>${bookObj.author}</p>
         <p>${bookObj.price}</p>`;
    fragment.append(li);
}

//Проверка книги на то, все ли поля есть
function checkBookInfo(book) {
    let requiredFields = ['author', 'name', 'price'];
    let bookKeys = Object.keys(book);
    let missingFields = requiredFields.filter(n => bookKeys.indexOf(n) === -1);

    try {
        // все поля есть
        if (missingFields.length === 0) {
            return book;
        } else {
            throw new Error(`Отсутсвует поле ${missingFields.join(', ')}`);
        }
    } catch (e) {
        console.log(e.message);
    }
}

let rootEl = document.querySelector('#root')
createList(rootEl, books);

