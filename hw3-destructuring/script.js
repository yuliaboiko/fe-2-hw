// Task1
console.group('Task1 ');

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

// Var1 with filter
let diffNames = clients2.filter(client => clients1.indexOf(client) == -1)
let generalClients = [...clients1, ...diffNames];

console.log(generalClients);

// Var2 with Set
let setNames = [...new Set([...clients1, ...clients2])];
console.log(setNames);
console.groupEnd();

// ----------------------------------------------------------------
// Task2

console.group('Task2');

const characters = [
    {
        name: "Елена",
        lastName: "Гилберт",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Кэролайн",
        lastName: "Форбс",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Аларик",
        lastName: "Зальцман",
        age: 31,
        gender: "man",
        status: "human"
    },
    {
        name: "Дэймон",
        lastName: "Сальваторе",
        age: 156,
        gender: "man",
        status: "vampire"
    },
    {
        name: "Ребекка",
        lastName: "Майклсон",
        age: 1089,
        gender: "woman",
        status: "vempire"
    },
    {
        name: "Клаус",
        lastName: "Майклсон",
        age: 1093,
        gender: "man",
        status: "vampire"
    }
];

function makeShortInfo({ name, lastName, age }) {
    return { name, lastName, age }
}

let charactersShortInfo = characters.map(personObj => makeShortInfo(personObj));
console.log(charactersShortInfo);
console.groupEnd();


// ----------------------------------------------------------------
// Task3

console.group('Task3');

const user1 = {
    name: "John",
    years: 30
};

let { name, years = age, isAdmin = false } = user1;
console.log(name);
console.log(years);
console.log(isAdmin);

console.groupEnd();


// ----------------------------------------------------------------
// Task4


console.group('Task4');

const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
        lat: 38.869422,
        lng: 139.876632
    }
}

const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
}

const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto',
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
}



let fullProfile = { ...satoshi2018, ...satoshi2019, ...satoshi2020 }
console.log(fullProfile);

console.groupEnd();

// ----------------------------------------------------------------
// Task5

console.group('Task5');

const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
}, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
}, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
}


let newBooks = [...books, bookToAdd];
console.log('books', books);
console.log('new books', newBooks);

console.groupEnd();


// ----------------------------------------------------------------
// Task6

console.group('Task6');

const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}

let newEmployee = { ...employee, age: 30, salary: 5000 };

console.log('employee', employee);
console.log('newEmployee', newEmployee);

console.groupEnd();

// ----------------------------------------------------------------
// Task7

console.group('Task7');

const array = ['value', () => 'showValue'];

[value, showValue] = array;

console.log(value);
console.log(showValue());


console.groupEnd();